import materialInput from './modules/components/materialInput';
import profile from './modules/controller';
import profileImage from './images/profile_image.jpg';
import './styles/main.scss';

const data = {
  userInfo: {
    firstName: 'Jessica',
    lastName: 'Parker',
    address: 'Newport Beach, CA',
    tel: '(949) 325 - 68594',
    website: 'www.seller.com',
    image: profileImage,
    social: {
      reviews: {
        number: 6,
        stars: 4,
        link: '#',
      },
      followers: {
        number: 15,
        link: '#',
      },
    },
  },
};

materialInput.init();
profile.init(data);
