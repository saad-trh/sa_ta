import DOMHelpers from '../helpers/DOMHelpers';
import pages from '../pages';
import materialInput from '../components/materialInput';

const profileView = (() => {
  const {
    createElement,
    on,
    off,
    getElement,
    getElements,
    addClass,
    removeClass,
    empty,
  } = DOMHelpers();

  const state = {
    firstName: '',
    lastName: '',
    address: '',
    tel: '',
    website: '',
  };

  const addName = (fullName) => {
    const nameNode = getElement('.profile-info__name');
    fullName ? (nameNode.textContent = fullName) : (nameNode.textContent = 'Anonymous');
  };

  const addTel = (tel) => {
    const telNode = getElement('.profile-info__tel span');
    tel ? (telNode.textContent = tel) : (telNode.parentNode.style.display = 'none');
  };

  const addAddress = (address) => {
    const addressNode = getElement('.profile-info__address span');
    address ? (addressNode.textContent = address) : (addressNode.parentNode.style.display = 'none');
  };

  const addImage = (image) => {
    const imageNode = getElement('.profile-info__image-container');

    if (image) {
      imageNode.style.backgroundImage = `url("${image}")`;
    } else {
      imageNode.style.backgroundImage = 'url("https://placekitten.com/g/300/250")';
    }
  };

  const addReviewsNumber = (reviewsNumber) => {
    const reviewsNumberNode = getElement('.feedback__number');
    reviewsNumberNode.textContent = reviewsNumber;

    if (reviewsNumber < 2) reviewsNumberNode.nextSibling.textContent = ' Review';
    else reviewsNumberNode.nextSibling.textContent = ' Reviews';
  };

  const addStars = (stars) => {
    const starNodes = getElements('.feedback__star .icon--star');

    starNodes.forEach((star, index) => {
      if (stars > index) {
        star.setAttribute('name', 'star');
      } else {
        star.setAttribute('name', 'star-outline');
      }
    });
  };

  const addReviewsLink = (link) => {
    const linkNode = getElement('.feedback__reviews .link');
    linkNode.href = link;
  };

  const addFollowersNumber = (followersNumber) => {
    const followersNumberNode = getElement('.followers__number');
    followersNumberNode.textContent = followersNumber;

    if (followersNumber < 2) followersNumberNode.nextSibling.textContent = ' Follower';
    else followersNumberNode.nextSibling.textContent = ' Followers';
  };

  const addFollowersLink = (link) => {
    const linkNode = getElement('.followers__text.link');
    linkNode.href = link;
  };

  const initSelectedPage = (firstName, lastName, website, tel, address) => {
    const selectedPage = pages.find((page) => page().isSelected);
    const { title, setContent } = selectedPage();
    const pageTitleNode = getElement('.heading--alpha');
    pageTitleNode.textContent = title;
    const editButtonNode = getElement('.edit-profile');
    const mainContentNode = getElement('.main__content');

    state.firstName = firstName;
    state.lastName = lastName;
    state.website = website;
    state.tel = tel;
    state.address = address;

    if (title === 'about') {
      editButtonNode.style.display = '';
      mainContentNode.insertAdjacentHTML(
        'beforeEnd',
        setContent(firstName, lastName, website, tel, address),
      );
    } else {
      editButtonNode.style.display = 'none';
      mainContentNode.insertAdjacentHTML('beforeEnd', setContent());
    }
  };

  const resetEditSM = () => {
    const editButton = getElement('.edit-profile--sm');
    const editButtonGroup = getElement('.edit-profile__buttons');
    const container = getElement('.about-page');

    if (!editButtonGroup) return;

    container.style.display = '';
    const form = getElement('form');
    if (form) form.remove();
    editButton.style.display = '';
    editButtonGroup.remove();
  };

  const switchPages = (refreshOnSwitch) => {
    const menuListNode = getElement('.menu__list');

    const handleClick = (event) => {
      const { target } = event;
      const menuItem = target.closest('.menu__item');
      const previousItem = menuListNode.querySelector('.menu__item--active');

      if (!menuItem || menuItem === previousItem) return;

      const menuTitle = menuItem.querySelector('.menu__link').textContent;
      const previousTitle = previousItem.querySelector('.menu__link').textContent;

      if (previousTitle.toLocaleLowerCase() === 'about') resetEditSM();

      const selectedPage = pages.find(
        (page) => page().title.toLocaleLowerCase() === menuTitle.toLocaleLowerCase(),
      );
      const { title, setContent } = selectedPage();
      // Edit page title
      const pageTitleNode = getElement('.heading--alpha');
      pageTitleNode.textContent = title;
      const editButtonNode = getElement('.edit-profile');
      const mainContentNode = getElement('.main__content');
      empty(mainContentNode);

      if (title === 'about') {
        editButtonNode.style.display = '';
        mainContentNode.insertAdjacentHTML(
          'beforeEnd',
          setContent(state.firstName, state.lastName, state.website, state.tel, state.address),
        );
        refreshOnSwitch();
      } else {
        editButtonNode.style.display = 'none';
        mainContentNode.insertAdjacentHTML('beforeEnd', setContent());
      }

      removeClass(previousItem, 'menu__item--active');
      addClass(menuItem, 'menu__item--active');
    };
    on(menuListNode, 'click', handleClick);
  };

  // Helper function - Create a text input field
  const createEditProfileForm = () => {
    const container = createElement('div', '.about-page--edit-mode');
    container.insertAdjacentHTML(
      'beforeEnd',
      `
      <form>
        <div class="input-container">
          <label for="firstName" class="input-container__label">First name</label>
          <input type="text" class="input-text" id="firstName" value="${state.firstName}" />
        </div>
        <div class="input-container">
          <label for="lastName" class="input-container__label">Last name</label>
          <input type="text" class="input-text" id="lastName" value="${state.lastName}" />
        </div>
        <div class="input-container">
          <label for="website" class="input-container__label">Website</label>
          <input type="text" class="input-text" id="website" value="${state.website}" />
        </div>
        <div class="input-container">
          <label for="phoneNumber" class="input-container__label">Phone number</label>
          <input type="text" class="input-text" id="phoneNumber" value="${state.tel}" />
        </div>
        <div class="input-container">
          <label for="address" class="input-container__label">City, state & zip</label>
          <input type="text" class="input-text" id="address" value="${state.address}" />
        </div>
      </form>
    `,
    );

    return container;
  };

  // Helper function - create save/cancel buttons
  const createEditButtons = () => {
    const container = createElement('div', '.edit-profile__buttons');
    const saveButton = createElement('div', '.button');
    const cancelButton = createElement('div', '.button');
    addClass(saveButton, 'button--text', 'save-button');
    addClass(cancelButton, 'button--text', 'cancel-button');
    saveButton.textContent = 'Save';
    cancelButton.textContent = 'Cancel';
    container.append(cancelButton, saveButton);

    return container;
  };

  // Update data
  const updateProfileInfo = (firstName, lastName, website, tel, address) => {
    const profileInfoName = getElement('.profile-info__name');
    const profileInfoTel = getElement('.profile-info__tel span');
    const profileInfoAddress = getElement('.profile-info__address span');
    const heading = getElement('.heading--beta');
    const aboutWebsite = getElement('.about-page__website span');
    const aboutTel = getElement('.about-page__tel span');
    const aboutAddress = getElement('.about-page__address span');

    profileInfoName.textContent = `${firstName} ${lastName}`;
    profileInfoTel.textContent = tel;
    profileInfoAddress.textContent = address;
    heading.textContent = `${firstName} ${lastName}`;
    aboutWebsite.textContent = website;
    aboutTel.textContent = tel;
    aboutAddress.textContent = address;

    state.firstName = firstName;
    state.lastName = lastName;
    state.website = website;
    state.tel = tel;
    state.address = address;
  };

  const editProfile = (firstName, lastName, website, tel, address, editModel) => {
    const editButton = getElement('.edit-profile--sm');
    const editButtonGroup = createEditButtons();
    const mainContent = getElement('.main__content');
    const mainHeader = getElement('.main__header');

    const handleClick = () => {
      const container = getElement('.about-page');
      const form = createEditProfileForm(firstName, lastName, website, tel, address);
      container.style.display = 'none';
      mainContent.append(form);
      editButton.style.display = 'none';
      mainHeader.append(editButtonGroup);
      const cancelButton = getElement('.cancel-button');
      const saveButton = getElement('.save-button');
      materialInput.updateInputFields();

      const handleCancelClick = () => {
        container.style.display = '';
        form.remove();
        editButton.style.display = '';
        editButtonGroup.remove();

        off(cancelButton, 'click', handleCancelClick);
      };

      const handleSaveClick = () => {
        const actualForm = form.querySelector('form');
        const firstNameUpdated = actualForm.elements.firstName.value;
        const lastNameUpdated = actualForm.elements.lastName.value;
        const websiteUpdated = actualForm.elements.website.value;
        const phoneNumberUpdated = actualForm.elements.phoneNumber.value;
        const addressUpdated = actualForm.elements.address.value;
        updateProfileInfo(
          firstNameUpdated,
          lastNameUpdated,
          websiteUpdated,
          phoneNumberUpdated,
          addressUpdated,
          editModel,
        );
        container.style.display = '';
        form.remove();
        editButtonGroup.replaceWith(editButton);
        editButton.style.display = '';
        editButtonGroup.remove();

        off(saveButton, 'click', handleSaveClick);
      };

      on(cancelButton, 'click', handleCancelClick);
      on(saveButton, 'click', handleSaveClick);
    };
    on(editButton, 'click', handleClick);
  };

  const createPopUp = (key) => {
    const oldPopup = getElement('.popup-block');

    if (oldPopup) oldPopup.remove();

    const container = createElement('div', '.popup-block');

    if (key !== 'fullName') {
      container.insertAdjacentHTML(
        'beforeend',
        `
        <div class="input-container">
          <label for="firstName" class="input-container__label">First name</label>
          <input type="text" class="input-text" id="firstName" value="${state[key]}" />
        </div>
        <div class="edit-profile__buttons">
        <div class="button button--contained save-button">Save</div>
          <div class="button button--outlined cancel-button">Cancel</div>
        </div>
      `,
      );
    } else {
      container.insertAdjacentHTML(
        'beforeend',
        `
        <div class="input-container">
          <label for="firstName" class="input-container__label">First name</label>
          <input type="text" class="input-text" id="firstName" value="${state.firstName}" />
        </div>
        <div class="input-container">
          <label for="lastName" class="input-container__label">First name</label>
          <input type="text" class="input-text" id="lastName" value="${state.lastName}" />
        </div>
        <div class="edit-profile__buttons">
          <div class="button button--contained save-button">Save</div>
          <div class="button button--outlined cancel-button">Cancel</div>
        </div>
      `,
      );
    }

    return container;
  };

  const editProfileLG = () => {
    const container = getElement('.about-page');

    const handleClick = (event) => {
      const { target } = event;
      const editButton = target.closest('.edit-profile--md');

      if (!editButton) return;

      const listItem = target.closest('.about-page__item');
      const headerName = target.closest('.about-page__name');
      let popup = null;
      let field = null;

      if (listItem) {
        field = listItem.id;
        popup = createPopUp(listItem.id);
        listItem.append(popup);
      } else if (headerName) {
        field = headerName.id;
        popup = createPopUp(headerName.id);
        headerName.append(popup);
      }

      materialInput.updateInputFields();
      const cancelButton = getElement('.cancel-button');
      const saveButton = getElement('.save-button');

      const handleBodyClick = (e) => {
        if (e.target.closest('.popup-block') !== popup && !e.target.closest('.edit-profile--md')) {
          popup.remove();
          off(document.body, 'click', handleBodyClick);
        }
      };

      const handleCancelClick = () => {
        popup.remove();
        off(cancelButton, 'click', handleCancelClick);
        off(document.body, 'click', handleBodyClick);
      };

      const handleSaveClick = () => {
        if (field === 'fullName') {
          state.firstName = getElement('#firstName').value;
          state.lastName = getElement('#lastName').value;
          const profileInfoName = getElement('.profile-info__name');
          const heading = getElement('.heading--beta');
          profileInfoName.textContent = `${state.firstName} ${state.lastName}`;
          heading.textContent = `${state.firstName} ${state.lastName}`;
        } else {
          state[field] = listItem.querySelector('input').value;

          switch (field) {
            case 'website':
              {
                const aboutWebsite = getElement('.about-page__website span');
                aboutWebsite.textContent = state[field];
              }
              break;
            case 'tel':
              {
                const profileInfoTel = getElement('.profile-info__tel span');
                const aboutTel = getElement('.about-page__tel span');
                profileInfoTel.textContent = state[field];
                aboutTel.textContent = state[field];
              }
              break;
            case 'address':
              {
                const profileInfoAddress = getElement('.profile-info__address span');
                const aboutAddress = getElement('.about-page__address span');
                profileInfoAddress.textContent = state[field];
                aboutAddress.textContent = state[field];
              }
              break;

            default:
              break;
          }
        }

        popup.remove();
        off(saveButton, 'click', handleSaveClick);
        off(document.body, 'click', handleBodyClick);
      };

      on(cancelButton, 'click', handleCancelClick);
      on(saveButton, 'click', handleSaveClick);
      on(document.body, 'click', handleBodyClick);
    };
    on(container, 'click', handleClick);
  };

  const loginPopup = () => {
    const loginButton = getElement('.auth__button');
    const loginBlock = getElement('.auth');

    const handleClick = (e) => {
      const oldPopup = getElement('.login-popup');
      if (e.target === oldPopup) return;

      if (oldPopup) {
        oldPopup.remove();
      } else {
        const popup = createElement('div', '.login-popup');
        popup.textContent = 'Do you want to login?';
        loginBlock.append(popup);
      }
    };

    const handleBodyClick = (e) => {
      const popup = getElement('.login-popup');

      if (e.target.closest('.login-popup') !== popup && !e.target.closest('.auth__button')) {
        popup.remove();
        // off(document.body, 'click', handleBodyClick);
      }
    };

    on(loginButton, 'click', handleClick);
    on(document.body, 'click', handleBodyClick);
  };

  const tooltip = () => {
    const followersButton = getElement('.followers .icon');
    const followers = getElement('.followers');
    const tooltipContainer = createElement('div', '.tooltip-container');
    tooltipContainer.textContent = 'Add follower';

    const handleMouseover = () => {
      followers.append(tooltipContainer);
    };

    const handleMouseleave = () => {
      tooltipContainer.remove();
    };

    on(followersButton, 'mouseover', handleMouseover);
    on(followersButton, 'mouseleave', handleMouseleave);
  };

  return {
    addName,
    addTel,
    addAddress,
    addImage,
    addReviewsNumber,
    addStars,
    addReviewsLink,
    addFollowersNumber,
    addFollowersLink,
    initSelectedPage,
    switchPages,
    editProfile,
    editProfileLG,
    loginPopup,
    tooltip,
  };
})();

export default profileView;
