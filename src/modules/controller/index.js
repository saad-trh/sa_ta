import profileModel from '../model';
import profileView from '../view';

const profile = (() => {
  const {
    addName,
    addTel,
    addAddress,
    addImage,
    addReviewsNumber,
    addStars,
    addReviewsLink,
    addFollowersNumber,
    addFollowersLink,
    initSelectedPage,
    switchPages,
    editProfile,
    editProfileLG,
    loginPopup,
    tooltip,
  } = profileView;

  const init = (data) => {
    const {
      getFirstName,
      getLastName,
      getFullName,
      getAddress,
      getTel,
      getWebsite,
      getImage,
      getReviewsNumber,
      getReviewsStars,
      getReviewsLink,
      getFollowersNumber,
      getFollowersLink,
      setFirstName,
      setLastName,
      setAddress,
      setTel,
      setWebsite,
    } = profileModel(data);

    const editModel = {
      setFirstName,
      setLastName,
      setAddress,
      setTel,
      setWebsite,
    };
    addName(getFullName());
    addTel(getTel());
    addAddress(getAddress());
    addImage(getImage());
    addReviewsNumber(getReviewsNumber());
    addStars(getReviewsStars());
    addReviewsLink(getReviewsLink());
    addFollowersNumber(getFollowersNumber());
    addFollowersLink(getFollowersLink());
    initSelectedPage(getFirstName(), getLastName(), getWebsite(), getTel(), getAddress());
    editProfile(getFirstName(), getLastName(), getWebsite(), getTel(), getAddress(), editModel);
    editProfileLG();
    loginPopup();
    tooltip();

    const refreshOnSwitch = () => {
      editProfileLG();
    };

    switchPages(refreshOnSwitch);
  };

  return {
    init,
  };
})();

export default profile;
