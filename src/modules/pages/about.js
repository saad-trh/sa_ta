const about = () => {
  const setContent = (firstName, lastName, website, tel, address) => `
    <div class="about-page">
    <div id="fullName" class="about-page__name">
      <h2 class="heading heading--beta">${firstName} ${lastName}</h2>
      <button class="edit-profile edit-profile--md">
        <ion-icon class="icon icon--pencil" name="pencil-sharp"></ion-icon>
      </button>
    </div>
    <ul class="about-page__user-info">
      <li id="website" class="about-page__item">
        <div class="about-page__info-block about-page__website">
          <ion-icon class="icon about-page__icon" name="earth"></ion-icon>
          <span>${website}</span>
        </div>
        <button class="edit-profile edit-profile--md">
          <ion-icon class="icon icon--pencil" name="pencil-sharp"></ion-icon>
        </button>
      </li>
      <li id="tel" class="about-page__item">
        <div class="about-page__info-block about-page__tel">
          <ion-icon class="icon about-page__icon" name="call-outline"></ion-icon>
          <span>${tel}</span>
        </div>
        <button class="edit-profile edit-profile--md">
          <ion-icon class="icon icon--pencil" name="pencil-sharp"></ion-icon>
        </button>
      </li>
      <li id="address" class="about-page__item">
        <div class="about-page__info-block about-page__address">
          <ion-icon class="icon about-page__icon" name="home-outline"></ion-icon>
          <span>${address}</span>
        </div>
        <button class="edit-profile edit-profile--md">
          <ion-icon class="icon icon--pencil" name="pencil-sharp"></ion-icon>
        </button>
      </li>
    </ul>
    </div>
  `;

  return {
    title: 'about',
    isSelected: true,
    setContent,
  };
};

export default about;
