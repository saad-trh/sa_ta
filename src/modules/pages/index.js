import about from './about';
import settings from './settings';
import option1 from './option1';
import option2 from './option2';
import option3 from './option3';

const pages = (() => [about, settings, option1, option2, option3])();

export default pages;
