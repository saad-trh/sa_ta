import DOMHelpers from '../../helpers/DOMHelpers';

const materialInput = (() => {
  const { on, addClass, removeClass } = DOMHelpers();

  const page = document.body;

  // Initialize component on page load / dynamically added input fields
  const updateInputFields = () => {
    const inputs = page.querySelectorAll('.input-container .input-text');

    inputs.forEach((input) => {
      if (input.value) {
        addClass(input.parentElement, 'input-container--input-has-value');
      }
    });
  };

  const init = () => {
    updateInputFields();

    const handleFocus = (event) => {
      const { target } = event;

      if (!target.classList.contains('input-text')) return;

      const inputContainer = target.closest('.input-container');

      addClass(inputContainer, 'input-container--input-focused');
    };

    const handleBlur = (event) => {
      const { target } = event;

      if (!target.classList.contains('input-text')) return;

      const inputContainer = target.closest('.input-container');
      removeClass(inputContainer, 'input-container--input-focused');

      if (!target.value) {
        removeClass(inputContainer, 'input-container--input-has-value');
      } else {
        addClass(inputContainer, 'input-container--input-has-value');
      }
    };

    on(page, 'focus', handleFocus, true);
    on(page, 'blur', handleBlur, true);
  };

  return {
    updateInputFields,
    init,
  };
})();

export default materialInput;
