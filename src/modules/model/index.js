const profileModel = (data) => {
  const { firstName, lastName, address, tel, website, image, social } = data.userInfo;
  const { reviews, followers } = social;

  const state = {
    firstName,
    lastName,
    address,
    tel,
    website,
  };

  const stringSanitize = (value) => {
    if (typeof value === 'string') return value;

    return '';
  };

  const numberSanitize = (value) => {
    if (typeof value === 'number' && value >= 0) return value;

    return 0;
  };

  const getFirstName = () => stringSanitize(state.firstName);

  const setFirstName = (value) => {
    if (value) state.firstName = value;
  };

  const getLastName = () => stringSanitize(state.lastName);

  const setLastName = (value) => {
    state.lastName = value;
  };

  const getFullName = () => `${getFirstName()} ${getLastName()}`;

  const getAddress = () => stringSanitize(state.address);

  const setAddress = (value) => {
    state.address = value;
  };

  const getTel = () => stringSanitize(state.tel);

  const setTel = (value) => {
    state.tel = value;
  };

  const getWebsite = () => stringSanitize(state.website);

  const setWebsite = (value) => {
    state.website = value;
  };

  const getImage = () => stringSanitize(image);

  const getReviewsNumber = () => numberSanitize(reviews.number);

  const getReviewsStars = () => {
    const stars = numberSanitize(reviews.stars);

    if (stars > 0 && stars <= 5) return stars;

    return 0;
  };

  const getReviewsLink = () => stringSanitize(reviews.link);

  const getFollowersNumber = () => numberSanitize(followers.number);

  const getFollowersLink = () => stringSanitize(followers.link);

  return {
    getFirstName,
    getLastName,
    getFullName,
    getAddress,
    getTel,
    getWebsite,
    getImage,
    getReviewsNumber,
    getReviewsStars,
    getReviewsLink,
    getFollowersNumber,
    getFollowersLink,
    setFirstName,
    setLastName,
    setAddress,
    setTel,
    setWebsite,
  };
};

export default profileModel;
